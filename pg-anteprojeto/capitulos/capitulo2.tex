\chapter{Fundamentação Teórica e Tecnologias Utilizadas}

Neste capitulo serão apresentados: a Engenharia Web e suas fases; o método FrameWeb, sua arquitetura de software e sua linguagem de modelagem; e também será feita uma breve introdução ao framework Play e a categoria de frameworks a qual ele pertence (frameworks MVC).

\section{Engenharia Web}
\label{eng-web}

Engenharia Web (\textit{Web Engineering - WebE}) é a Engenharia de Software aplicada ao desenvolvimento Web~\cite{pressmannSoftEng}. Ela estabelece abordagens disciplinadas e sistemáticas, através de um tom científico, engenharia e princípios de gestão, para garantir o bom desenvolvimento, implementação e manutenção de sistemas e aplicações baseadas em Web~\cite{murugesan2001web}.

A Internet passou a ser parte integral das nossas vidas: ela está presente nas indústrias, no governo, nos bancos, nas lojas (muitas das vezes, lojas completamente virtuais), etc... À medida que as pessoas dependem cada vez mais de sistemas e aplicativos baseados em Web, se torna imprescindível que esses apresentem um padrão alto de qualidade.

De modo geral, os atributos mais relevantes para a garantia de qualidade nas aplicações Web estão listados a seguir~\cite{olsina2001specifying}:

\begin{itemize}
    \item{Usabilidade: refere-se à facilidade de compreensão e uso do site para os usuários, incluindo aqueles com pouco conhecimento técnico e também os que possuem alguma deficiência. Para isso, deve-se empregar recursos de ajuda e feedback online e planejamento da interface nos aspectos estéticos e com características especiais voltadas a determinados usuários;}

    \item Funcionalidade: refere-se ao funcionamento correto do sistema, isso é, sua capacidade de busca e recuperação de informações, a adaptação aos diferentes \textit{browsers}, e ser capaz de executar as funções relacionadas ao domínio da aplicação do sistema;

    \item Confiabilidade: refere-se à garantia da validação dos dados de entrada dos usuários, recuperação de erros e funcionamento correto dos \textit{links} no site;

    \item Eficiência: refere-se a garantir uma velocidade satisfatória na geração de páginas, fazendo com que o tempo de resposta seja o menor possível;

    \item Manutenibilidade: refere-se à facilidade do sistema em ter seus erros corrigidos, se adaptar a modificações e estender seu uso a novas situações. Esse aspecto é fundamental devido à rápida evolução tecnológica e à necessidade de atualização constante de conteúdo na Web.
\end{itemize}    

A metodologia da Engenharia Web trabalha para que tais atributos sejam plenamente satisfeitos. O modelo proposto pela Engenharia Web é divido em várias fases e segue uma abordagem iterativa. As fases estão descritas a seguir.

\subsection{Análise de Requisitos}

A primeira fase da Engenharia Web é a Análise de Requisitos, etapa na qual tudo o que é considerado necessário para a entrega da aplicação é levantado por meio de consulta aos \textit{stakeholders}, i.e., todas as pessoas interessadas no sistema, incluindo donos e possíveis usuários. São coletadas as informações sobre os requisitos funcionais --- as funções que o sistema deve executar --- e sobre os requisitos não-funcionais --- as restrições sob as quais o sistema deve operar.

Nessa fase, os seguintes questionamentos tem que ser respondidos:

\begin{itemize}
    \item Quais são os objetivos visados pela \textit{WebApp}?
    \item Quem serão os usuários da \textit{WebApp}?
    \item Quais necessidades de negócio devem ser atendidas pela \textit{WebApp}?
\end{itemize}

\subsection{Modelo de Análise}

A próxima fase é o Modelo de Análise e foca-se principalmente nas seguintes categorias: \textbf{Conteúdo}, que identifica as classes de conteúdo que devem ser fornecidas para a \textit{WebApp}; \textbf{Interação}, que descreve a forma pelo qual o usuário interage com a \textit{WebApp}; \textbf{Funcional}, que define as operações aplicadas ao conteúdo da Webapp e a sequência de processamentos consequentes; \textbf{Navegação}, que define a estratégia geral de navegação para a \textit{WebApp}; e \textbf{Configuração}, que descreve o ambiente operacional e a infraestrutura na qual a \textit{WebApp} reside.

\subsection{Projeto da \textit{WebApp}}

Com base nos resultados do modelo de análise, a próxima fase da Engenharia Web é o Projeto da \textit{WebApp}, que é focado em seis tópicos fundamentais: \textbf{interface}, \textbf{estética}, \textbf{conteúdo}, \textbf{navegação}, \textbf{arquitetura} e {\textbf{componentes}.

\subsection{Implementação}

Na fase de implementação, o projeto é propriamente desenvolvido utilizando uma linguagem de programação adequada às suas características e requisitos. 

\subsection{Testes}

Por fim, a fase de testes visa garantir que a \textit{WebApp}  esteja correta, principalmente em termos de conteúdo (isto é, atende aos requisitos levantados), navegabilidade, segurança, carga, eficiência
e interoperabilidade entre diferentes navegadores Web.

\section{FrameWeb}

O FrameWeb~\cite{Souza2007} (\textit{Framework-based Design Method for Web Engineering}) é um método baseado em \textit{frameworks} para o desenvolvimento de sistemas de informação Web (\textit {Web Information System} – WISs). Dentre as diversas propostas para Engenharia Web, até então não havia nenhuma que pesasse diretamente os aspectos característicos dos \textit{frameworks} na construção de WISs, porém o FrameWeb apresenta essa abordagem. 

A Figura~\ref{frameweb1} ilustra o processo de desenvolvimento proposto.

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{figuras/frameweb1.png}
	\caption{Processo de desenvolvimento de software sugerido por FrameWeb~\cite{Souza2007}.}
	\label{frameweb1}
\end{figure}

A fase de Projeto concentra as propostas principais do método: 

\begin{itemize}
    \item{definição de uma arquitetura padrão que divide o sistema em camadas, para promover sua melhor integração com os \textit{frameworks} utilizados;}
    \item{proposição de conjunto de modelos de projeto que empregam conceitos utilizados pelos \textit{frameworks} por meio de um perfil UML que aproxima os diagramas da implementação}
\end{itemize}


Na fase de implementação a produção do código é agilizada pela utilização dos \textit{frameworks} e pela fidelidade que existe entre os modelos da fase de projeto e sua implementação.

\subsection{Arquitetura de software do FrameWeb}

A arquitetura lógica padrão para WISs utilizada no
FrameWeb é apresentada na Figura~\ref{arquitetura-software}

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{figuras/fig-arquitetura-software.jpg}
	\caption{Arquitetura padrão para WIS baseada no padrão arquitetônico ServiceLayer~\cite{fowler2002patterns}.}
	\label{arquitetura-software}
\end{figure}

O sistema é dividido em três camadas, que apresentam subdivisões internas em pacotes. 

\begin{itemize}
    \item Camada de \textbf{Lógica de Apresentação}: provê as interfaces gráficas para a interação do usuário com o sistema. Nela são apresentadas as informações das classes de domínio e o usuário faz suas requisições.
        \begin{itemize}
            \item Pacote de \textbf{Visão}: é onde estão páginas Web, imagens e scripts que são executados do lado do cliente, e também os arquivos exclusivamente relacionamentos com apresentação de informações aos usuários.
            \item Pacote de \textbf{Controle}: é responsável pelo monitoramento dos estímulos enviados pelo cliente através dos elementos da Visão, e também abrange as classes de controle e outros arquivos relacionados ao \textit{framework} Controlador Frontal.
        \end{itemize}
    \item Camada de \textbf{Lógica de Negócio}:
        \begin{itemize}
            \item Pacote de \textbf{Domínio}: nele encontram-se as classes que representam o domínio do problema, já modeladas nos diagramas de classe na fase de análise.
            \item Pacote de \textbf{Aplicação}: nele estão implementados os casos de uso, provendo uma camada de serviços independente da interface com o usuário.
        \end{itemize}
    \item Camada de \textbf{Lógica de Acesso a Dados}:
        \begin{itemize}
            \item Pacote de \textbf{Persistência}: contém as classes responsáveis por armazenar os objetos persistentes em um banco de dados.
        \end{itemize}

    \end{itemize}

Acerca das relações de dependência entre pacotes, tem-se que:

\begin{itemize}
    \item O pacote de \textbf{Aplicação} manipula objetos do pacote de Domínio;
    \item O pacote de \textbf{Aplicação}, por meio do pacote \textbf{Persistência}, recupera, grava, altera e exclui objetos de domínio de acordo com a execução dos casos de uso;
    \item Elementos do Pacote de \textbf{Visão} enviam os estímulos do usuário (clique de um botão, preenchimento de um campo texto, acionamento de uma caixa de seleção, etc.) para o Pacote de \textbf{Controle}, que por sua vez responde aos estímulos;
    \item O pacote \textbf{Controle} possui dependência com o pacote \textbf{Aplicação}, que provê ao usuário acesso as principais funcionalidades;
    \item Os pacotes \textbf{Visão, Controle e Persistência} possuem uma dependência fraca com o pacote de \textbf{Domínio}: não fazem alterações nos objetos de domínio, apenas os exibem ou os usam para realizar mapeamento do banco de dados relacional.
\end{itemize}

\subsection{Linguagem de modelagem de FrameWeb}

O método FrameWeb apresenta uma linguagem de modelagem
baseada em UML cujo propósito é guiar a implementação das camadas explicadas na seção anterior, a fim de representar componentes  utilizados no desenvolvimento Web e  componentes relacionados com o uso de \textit{frameworks}. A proposta é criar quatro diagramas:

\begin{itemize}
    \item O \textbf{Modelo de Entidades} representa os objetos que fazem parte do domínio do problema e seu mapeamento para a persistência. Ele é desenvolvido a partir do diagrama de classes criado na fase de análise.
    \item O \textbf{Modelo de Persistência} representa os objetos que são responsáveis pela persistência dos dados gerados pelo sistema; isto é, guia a implementação das classes DAO do sistema, as interfaces e suas implementações, juntamente com os métodos específicos de cada uma.
    \item O \textbf{Modelo de Aplicação} representa as classes de serviço que são responsáveis por implementar a lógica de negócio do sistema, e suas dependências.
    \item O \textbf{Modelo de Navegação} representa as páginas Web do sistema e seus atributos, que interagem com a camada de controle do sistema para  administrar os estímulos mandados e recebidos do usuário.
    
\end{itemize}

Uma descrição mais completa do método FrameWeb pode ser lida em~\cite{Souza2007}.

\section{Framework Play}

Play é um \textit{framework} open-source para aplicações Web, escrito em Scala mas também usável em Java, de alta produtividade, que integra componentes e APIs para o desenvolvimento moderno de aplicativos da Web~\cite{Playdocumentation}.

Play pertence a categoria de frameworks MVC.

\subsection{Frameworks MVC}

Para garantir a manutenibilidade e permitir maior escalabilidade, é fundamental que \textit{WebApps} complexos façam uma separação entre os dados (Model) e o layout (View). Desse modo, mudanças realizadas no layout das páginas não afetam os dados, e mudanças nos dados não afetam o layout.

O MVC resolve este problema através da separação das tarefas de acesso aos dados e lógica de negócio, lógica de apresentação e de interação com o utilizador, introduzindo um componente entre os dois: o controlador.

A Figura~\ref{mvc1} ilustra a arquitetura MVC.

\begin{figure}
	\centering
	\includegraphics[width=.7\textwidth]{figuras/fig-padrao-mvc.jpg}
	\caption{Diagrama representando a arquitetura MVC.}
	\label{mvc1}
\end{figure}

Os elementos da View (Visão) representam as informações do Model (Modelo) para os Usuários (Users), que, a partir disso, interagem com a aplicação. Essa interação é tratada pelo Controller (Controlador), capaz de modificar elementos do Modelo, e também notifica a Visão dessas alterações, que então atualizar a informação apresentada ao usuário.

Porém, na plataforma Web a arquitetura MVC precisa ser levemente adaptada, a esse padrão aplicado a Web dá-se o nome de \textit{Front Controller} (Controlador Frontal), e sua arquitetura é ilustrada na Figura~\ref{mvc2}

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{figuras/fig-controlador-frontal-web.jpg}
	\caption{Representação de um Controlador Frontal na Web~\cite{Souza2007}.}
	\label{mvc2}
\end{figure}

O navegador apresenta as páginas para o cliente, que faz uma requisição HTTP --- um pedido de leitura de uma página ou envio de dados para processamento --- ao servidor. O servidor Web encarrega o controlador frontal de tratar a requisição e este passa a gerenciar todo o processo. Então é emitida uma resposta apropriada à requisição e exibida para o cliente pelo navegador. Os \textit{frameworks} MVC fornecem um controlador frontal a ser configurado pelo desenvolvedor para melhor adaptação ao seu projeto.

\label{capitulo2}
