package br.ufes.scap.services

import br.ufes.scap.persistence.SolicitacaoDAOSlick
import br.ufes.scap.models.{Solicitacao, SolicitacaoFull, 
  User, StatusSolicitacao, TipoAcessorio}
import java.time.{LocalDate, LocalDateTime, ZoneId}
import java.sql.Timestamp

abstract class SolicitacaoService {

  def addSolicitacao(solicitacao: Solicitacao) 

  def deleteSolicitacao(id: Long) 

  def getSolicitacao(id: Long): SolicitacaoFull 

  def listAllSolicitacoes: Seq[SolicitacaoFull] 
  
  def turnSeqSolIntoSeqSolFull(solicitacoes : Seq[Solicitacao]): Seq[SolicitacaoFull] 
  
  def listAllSolicitacoesById(id : Long): Seq[SolicitacaoFull] 
  
  def listAllSolicitacoesBySolicitante(idProfessor : Long): Seq[SolicitacaoFull]
  
  def listAllSolicitacoesByRelator(idRelator : Long): Seq[SolicitacaoFull] 
  
  def listAllSolicitacoesByStatus(status : String): Seq[SolicitacaoFull] 
  
  def update(solicitacao : SolicitacaoFull) 
  
  def mergeListas(listSolicitacao1 : Seq[SolicitacaoFull], listSolicitacao2 : Seq[SolicitacaoFull]) : Seq[SolicitacaoFull] 

  def mudaStatus(oldSolicitacao : SolicitacaoFull, status : String) 
  
  def turnSolicitacaoIntoSolicitacaoFull
  (oldSolicitacao : Solicitacao): SolicitacaoFull 
  
  def turnSolicitacaoFullIntoSolicitacao
  (oldSolicitacao : SolicitacaoFull): Solicitacao 
  
  def addRelator(oldSolicitacao : SolicitacaoFull, idRelator : Long): SolicitacaoFull 
  
  def cancelaSolicitacao(oldSolicitacao : SolicitacaoFull): SolicitacaoFull 
  
  def busca(idProfessor : Long, idRelator : Long, status : String): Seq[SolicitacaoFull] 
}