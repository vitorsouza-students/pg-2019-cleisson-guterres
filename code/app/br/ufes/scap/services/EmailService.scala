package br.ufes.scap.services

import br.ufes.scap.models.{Global,User,SolicitacaoFull}

trait EmailService {
 
   def sendEmail(user : User, assunto : String, message : String) 
   
  def enviarEmailParaRelator(sol : SolicitacaoFull, relator : User) 
          
   def enviarEmailParaChefeCancelamento(sol : SolicitacaoFull) 
    
   def enviarEmailParaSolicitante(sol : SolicitacaoFull, status : String) 
    
   def enviarEmailParaTodos() 
   
}