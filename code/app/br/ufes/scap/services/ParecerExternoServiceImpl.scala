package br.ufes.scap.services

import br.ufes.scap.persistence.ParecerExternoDAOSlick
import br.ufes.scap.models.{ParecerExterno, ParecerExternoFull}

object ParecerExternoServiceImpl extends ParecerExternoService {
  
  def addParecer(parecer: ParecerExterno) = {
    ParecerExternoDAOSlick.save(parecer)
  }

  def deleteParecer(id: Long) = {
    ParecerExternoDAOSlick.delete(id)
  }

  def getParecer(id: Long): ParecerExternoFull = {
    turnParecerIntoParecerFull(ParecerExternoDAOSlick.get(id).get)
  }

  def listAllPareceres: Seq[ParecerExternoFull] = {
    turnSeqParecerIntoSeqParecerFull(ParecerExternoDAOSlick.listAll)
  }
  
  def listAllPareceresBySolicitacao(idSolicitacao : Long): Seq[ParecerExternoFull] = {
    turnSeqParecerIntoSeqParecerFull(ParecerExternoDAOSlick.findBySolicitacao(idSolicitacao))
  }
    
  def update(parecer : ParecerExterno) = { 
    ParecerExternoDAOSlick.update(parecer)
  }
  
    def turnSeqParecerIntoSeqParecerFull(parentescos : Seq[ParecerExterno]) : Seq[ParecerExternoFull] = {
    var Pareceres : Seq[ParecerExternoFull] = Seq()
    for (p <- parentescos){
       Pareceres = Pareceres :+ turnParecerIntoParecerFull(p)
    }
    return Pareceres
  }
    
  def turnParecerIntoParecerFull(oldParecer : ParecerExterno): ParecerExternoFull = {
    val sol = SolicitacaoServiceImpl.getSolicitacao(oldParecer.idSolicitacao) ;
    val dataParecer = SharedServicesImpl.getData(oldParecer.dataParecer)
    return ParecerExternoFull(oldParecer.id, sol, oldParecer.tipo, oldParecer.julgamento, oldParecer.fileData, dataParecer) 
  }

}