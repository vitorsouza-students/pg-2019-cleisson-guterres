package br.ufes.scap.services

import br.ufes.scap.persistence.MandatoDAOSlick
import br.ufes.scap.models.{Mandato, MandatoFull}
import java.time.{LocalDate, LocalDateTime, ZoneId}
import java.sql.Timestamp
import java.util.Date

trait MandatoService {

  def addMandato(mandato: Mandato) 

  def deleteMandato(id: Long) 
  
  def getMandato(id: Long): Option[Mandato]
  
  
  def getMandatoFull(id: Long): MandatoFull 

  def listAllMandatos: Seq[MandatoFull] 
  
  def listAllMandatosByProfessor(idProfessor : Long): Seq[MandatoFull] 
    
  def update(mandato : Mandato) 
  
  def getMandatoAtual(): Seq[MandatoFull] 
    
  def turnMandatoIntoMandatoFull(oldMandato : Mandato): MandatoFull 
  
  def turnSeqMandatosIntoSeqMandatosFull(mandatos : Seq[Mandato]): Seq[MandatoFull] 
  
  def checaDataOutros(dataInicial : Date, dataFinal : Date, cargo : String): Boolean 
  
}