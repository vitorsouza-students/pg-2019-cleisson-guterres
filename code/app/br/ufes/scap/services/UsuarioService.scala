package br.ufes.scap.services

import br.ufes.scap.persistence.UserDAOSlick
import br.ufes.scap.models.User

abstract class UsuarioService {

  def addUser(user: User) 

  def deleteUser(id: Long) 

  def getUser(id: Long): Option[User]

  def listAllUsers: Seq[User] 
  
  def listAllUsersByTipo(tipo: String): Seq[User]
  
  def update(user : User) 
  
  def getUserByMatricula(matricula : String): Option[User] 
  
  def checaMatriculaSenha(matricula : String, password : String): Boolean 
  
  def checaMatricula(matricula : String): Boolean 
  
}
