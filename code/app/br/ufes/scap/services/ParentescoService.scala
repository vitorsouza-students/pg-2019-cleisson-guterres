package br.ufes.scap.services

import br.ufes.scap.models.{Parentesco, ParentescoFull}

abstract class ParentescoService {

  def addParentesco(parentesco: Parentesco) 

  def deleteParentesco(id: Long) 

  def getParentesco(id: Long): Option[Parentesco] 

  def listAllParentescos: Seq[ParentescoFull] 
  
  def listAllParentescosByProfessor(idProfessor : Long): Seq[ParentescoFull] 
    
  def update(parentesco : Parentesco) 
  
  def checaDiferente(id1 : Long, id2 : Long):Boolean 
  
  def turnParentescoIntoParentescoFull(p : Parentesco) : ParentescoFull 
  
  def turnSeqParentescoIntoSeqParentescoFull(parentescos : Seq[Parentesco]) : Seq[ParentescoFull] 
          
  def naoExisteParentesco(id1 : Long, id2 : Long):Boolean 
  
}
