package br.ufes.scap.services

import br.ufes.scap.persistence.ParecerExternoDAOSlick
import br.ufes.scap.models.{ParecerExterno, ParecerExternoFull}

trait ParecerExternoService {
  
  def addParecer(parecer: ParecerExterno) 

  def deleteParecer(id: Long) 

  def getParecer(id: Long): ParecerExternoFull 

  def listAllPareceres: Seq[ParecerExternoFull] 
  
  def listAllPareceresBySolicitacao(idSolicitacao : Long): Seq[ParecerExternoFull]
    
  def update(parecer : ParecerExterno) 
  
  def turnSeqParecerIntoSeqParecerFull(parentescos : Seq[ParecerExterno]) : Seq[ParecerExternoFull] 
    
  def turnParecerIntoParecerFull(oldParecer : ParecerExterno): ParecerExternoFull 

}