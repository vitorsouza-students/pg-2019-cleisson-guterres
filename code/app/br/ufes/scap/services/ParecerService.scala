package br.ufes.scap.services

import br.ufes.scap.models.{Parecer, ParecerFull}

abstract class ParecerService {

  def addParecer(parecer: Parecer) 

  def deleteParecer(id: Long) 
  
  def getParecer(id: Long): ParecerFull 

  def listAllPareceres: Seq[ParecerFull]
  
  def listAllPareceresBySolicitacao(idSolicitacao : Long):Seq[ParecerFull] 
    
  def update(parecer : Parecer) 
  
  def turnSeqParecerIntoSeqParecerFull(pareceres : Seq[Parecer]) : Seq[ParecerFull] 
    
  def turnParecerIntoParecerFull(oldParecer : Parecer): ParecerFull 
  
}
