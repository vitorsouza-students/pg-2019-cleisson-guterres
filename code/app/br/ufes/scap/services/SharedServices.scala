package br.ufes.scap.services

import java.time.{LocalDate, LocalDateTime, ZoneId}
import java.sql.Timestamp
import java.util.Date
import br.ufes.scap.models.Global

trait SharedServices {
  
  def getData(data : Timestamp): LocalDate 
  
  def getDataTime(data : Timestamp): LocalDateTime 
  
  def checaDataFromSolicitacao(dataIni : Date, idSolicitacao : Long): Boolean 
  
  def checaData(dataInicio : Date, dataFim : Date): Boolean 
    
}
