package br.ufes.scap.persistence

import br.ufes.scap.models.{ParecerExterno}

trait ParecerExternoDAO extends BaseDAO {
  
  def findBySolicitacao(idSolicitacao : Long): Seq[ParecerExterno]
  

}
