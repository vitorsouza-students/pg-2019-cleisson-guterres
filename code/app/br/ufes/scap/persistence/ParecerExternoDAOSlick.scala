package br.ufes.scap.persistence

import br.ufes.scap.models.{ParecerExterno, ParecerExternoForm, ParecerExternoTableDef}
import play.api.Play
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent._
import scala.concurrent.duration._

object ParecerExternoDAOSlick extends ParecerExternoDAO {
  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  val pareceresDocumento = TableQuery[ParecerExternoTableDef]
  
  @Override
  def  save(p: Any) = {
        val ParecerExterno = p.asInstanceOf[ParecerExterno]
    dbConfig.db.run(pareceresDocumento += ParecerExterno).map(res => 
      "Parecer successfully added").recover {
      case ex: Exception => 
        println(ex.printStackTrace())
        ex.getClass().getName()
    }
  }

  @Override
  def delete(id: Long) = {
    dbConfig.db.run(pareceresDocumento.filter(_.id === id).delete)
  }

  @Override
  def get(id: Long): Option[ParecerExterno] = {
    Await.result(dbConfig.db.run(pareceresDocumento.filter(_.id === id).result.headOption),Duration.Inf)
  }
    
  @Override
  def update(p: Any) = {
    val ParecerExterno = p.asInstanceOf[ParecerExterno]
    dbConfig.db.run(pareceresDocumento.filter(_.id === ParecerExterno.id).update(ParecerExterno)).map(res => "Parecer successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def findBySolicitacao(idSolicitacao : Long): Seq[ParecerExterno] = {
    Await.result(dbConfig.db.run(pareceresDocumento.filter(_.idSolicitacao === idSolicitacao).result),Duration.Inf)
  }
  
  
  def listAll: Seq[ParecerExterno] = {
    Await.result(dbConfig.db.run(pareceresDocumento.result),Duration.Inf)
  }


}