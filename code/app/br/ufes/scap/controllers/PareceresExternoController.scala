package br.ufes.scap.controllers

import br.ufes.scap.models.{Global, ParecerExterno, 
 ParecerExternoForm, 
  StatusSolicitacao, TipoJulgamento, Setor}
import play.api.mvc._
import br.ufes.scap.services.{ParecerExternoServiceImpl, 
  SolicitacaoServiceImpl, EmailServiceImpl}
import scala.concurrent.ExecutionContext.Implicits.global
import javax.inject.Inject
import scala.concurrent.Future
import java.sql.Timestamp
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.Calendar
import java.nio.file.{Files, Paths}
import java.io.FileOutputStream;
import java.io.File;

class PareceresExternoController @Inject() 
(authenticatedUsuarioAction: AuthenticatedUsuarioAction,
    authenticatedSecretarioAction : AuthenticatedSecretarioAction)  extends Controller { 

  def registrarParecerExternoForm(idSolicitacao : Long) = authenticatedSecretarioAction { implicit request =>
    val solicitacao = SolicitacaoServiceImpl.getSolicitacao(idSolicitacao)
      Ok(br.ufes.scap.views.html.addParecerExterno(ParecerExternoForm.form, solicitacao))
  }
  
  def registrarParecerExterno(idSolicitacao : Long) = authenticatedSecretarioAction.async { implicit request =>
    ParecerExternoForm.form.bindFromRequest.fold(
        errorForm => Future.successful
        (
            BadRequest
            (br.ufes.scap.views.html.addParecerExterno
              (errorForm, SolicitacaoServiceImpl.getSolicitacao(idSolicitacao))
            )
        ),
        data => {
          val solicitacao = SolicitacaoServiceImpl.getSolicitacao(idSolicitacao)
              val dataAtual = new Timestamp(Calendar.getInstance().getTime().getTime())
              val byteArray = Files.readAllBytes(Paths.get(data.filePath))
              val newParecerExterno = ParecerExterno(0, idSolicitacao, data.tipo, data.julgamento, byteArray, dataAtual)   
              if (data.julgamento.equals(TipoJulgamento.Contra.toString())){
                  SolicitacaoServiceImpl.mudaStatus(solicitacao,StatusSolicitacao.Reprovada.toString())
                  EmailServiceImpl.enviarEmailParaSolicitante(solicitacao, StatusSolicitacao.Reprovada.toString())
              }else{
                if (data.julgamento.equals(TipoJulgamento.AFavor.toString()) && data.tipo.equals(Setor.CT.toString())){
                  SolicitacaoServiceImpl.mudaStatus(solicitacao,StatusSolicitacao.AprovadaCT.toString())
                  EmailServiceImpl.enviarEmailParaSolicitante(solicitacao, StatusSolicitacao.AprovadaCT.toString())
                }else{
                   if (data.julgamento.equals(TipoJulgamento.AFavor.toString()) && data.tipo.equals(Setor.PRPPG.toString())){
                     SolicitacaoServiceImpl.mudaStatus(solicitacao,StatusSolicitacao.AprovadaPRPPG.toString())
                     EmailServiceImpl.enviarEmailParaSolicitante(solicitacao, StatusSolicitacao.AprovadaPRPPG.toString())
                   }
                }
              }
              ParecerExternoServiceImpl.addParecer(newParecerExterno)
              Future.successful(  Redirect(routes.LoginController.menu())
              )
        }
    )
    }
  
    def verParecer(idParecer: Long) = authenticatedUsuarioAction{
      val parecer = ParecerExternoServiceImpl.getParecer(idParecer)
      Ok(br.ufes.scap.views.html.verParecerExterno(parecer))
    }
  
    def DownloadFile(idParecer : Long) = authenticatedUsuarioAction{
          val ParecerExterno = ParecerExternoServiceImpl.getParecer(idParecer)
          val currentDirectory = new java.io.File(".").getCanonicalPath
          val output = new FileOutputStream(new File(currentDirectory + "/Pareceres/" + "PARECER-" + ParecerExterno.tipo + "_ID-" + ParecerExterno.id));
          System.out.println("Getting file please be patient..");
    
          output.write(ParecerExterno.fileData);
          
          println("File writing complete !")
          Ok(br.ufes.scap.views.html.downloadSucesso())
      }
  
  }
