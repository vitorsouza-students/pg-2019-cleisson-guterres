package br.ufes.scap.controllers

import br.ufes.scap.models.{Global, User, UserForm, Solicitacao,
SolicitacaoFull, ManifestacaoForm, EncaminhamentoForm, 
 SolicitacaoForm, StatusSolicitacao,
TipoUsuario, BuscaForm, TipoAfastamento, TipoAcessorio}
import play.api.mvc._
import br.ufes.scap.services.{SolicitacaoServiceImpl, UsuarioServiceImpl, 
  EmailServiceImpl}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import java.sql.Timestamp
import java.sql.Date
import java.util.Calendar
import java.text.SimpleDateFormat
import javax.inject.Inject

class SolicitacoesController @Inject()
(authenticatedUsuarioAction: AuthenticatedUsuarioAction,
    authenticatedSecretarioAction : AuthenticatedSecretarioAction,
    authenticatedProfessorAction : AuthenticatedProfessorAction,
    authenticatedChefeAction : AuthenticatedChefeAction) extends Controller { 
  
  
  def definirBuscaForm = authenticatedUsuarioAction { 
        val users = UsuarioServiceImpl.listAllUsersByTipo(TipoUsuario.Prof.toString())
        Ok(br.ufes.scap.views.html.buscarSolicitacoes(BuscaForm.form, users))
  }
  
  def showAfastamentosByProfessor(idProfessor : Long) = authenticatedUsuarioAction {
    val afastamentos = SolicitacaoServiceImpl.listAllSolicitacoesBySolicitante(idProfessor)
    Ok(br.ufes.scap.views.html.listSolicitacoes(afastamentos))
  }
  
  def definirBusca = authenticatedUsuarioAction.async { implicit request =>
        BuscaForm.form.bindFromRequest.fold(
        // if any error in submitted data
        errorForm => 
          Future.successful 
          (BadRequest(br.ufes.scap.views.html.buscarSolicitacoes(errorForm, UsuarioServiceImpl.listAllUsersByTipo(TipoUsuario.Prof.toString())))),
        buscaForm => {
          val solicitacoes = SolicitacaoServiceImpl.busca(buscaForm.idProfessor, buscaForm.idRelator, buscaForm.status)
          Future.successful(Ok(br.ufes.scap.views.html.listSolicitacoes(solicitacoes)))
        }
        )
  }
  
  def listarSolicitacoes = authenticatedUsuarioAction { 
       val solicitacoes = SolicitacaoServiceImpl.listAllSolicitacoes
       Ok(br.ufes.scap.views.html.listSolicitacoes(solicitacoes))
  }

  def cancelarSolicitacao(id: Long) = authenticatedProfessorAction { implicit request =>
      val sol = SolicitacaoServiceImpl.getSolicitacao(id)
      if (Global.isAutor(sol.professor)){
        val newSolicitacao = SolicitacaoServiceImpl.cancelaSolicitacao(sol)
        SolicitacaoServiceImpl.update(newSolicitacao)
        EmailServiceImpl.enviarEmailParaChefeCancelamento(newSolicitacao)
            Redirect(routes.LoginController.menu())
      }else{
            Ok(br.ufes.scap.views.html.erro())
      }
  }
  
  def verSolicitacao(id : Long) = authenticatedUsuarioAction { implicit request =>
    var userTipo = Global.SESSION_TIPO
    val solicitacao = SolicitacaoServiceImpl.getSolicitacao(id)
    if(Global.isAutor(solicitacao.professor)){
      userTipo = TipoAcessorio.Autor.toString()
    }
    if(Global.isRelator(solicitacao.relator) 
        && solicitacao.tipoAfastamento.equals(TipoAfastamento.Internacional.toString())){
      userTipo = TipoAcessorio.Relator.toString()
    }
    if(Global.isChefe()
        && solicitacao.tipoAfastamento.equals(TipoAfastamento.Internacional.toString())){
      userTipo = TipoAcessorio.Chefe.toString()
    }
    if(Global.isSecretario()
        && solicitacao.tipoAfastamento.equals(TipoAfastamento.Internacional.toString())){
      userTipo = TipoUsuario.Sec.toString()
    }
    Ok(br.ufes.scap.views.html.verSolicitacao(solicitacao, userTipo))
  }
  
  def arquivar(id : Long)  = authenticatedSecretarioAction {
    val oldSolicitacao = SolicitacaoServiceImpl.getSolicitacao(id)
    val solicitacao = SolicitacaoServiceImpl.mudaStatus(oldSolicitacao, StatusSolicitacao.Arquivada.toString())
    Redirect(routes.LoginController.menu())
  }
  
  def addSolicitacaoForm() = authenticatedProfessorAction{
    Ok(br.ufes.scap.views.html.addSolicitacao(SolicitacaoForm.form))
  }
  
  def aprovar(id : Long) = authenticatedSecretarioAction {
    val sol = SolicitacaoServiceImpl.getSolicitacao(id)
    SolicitacaoServiceImpl.mudaStatus(sol, StatusSolicitacao.AprovadaDI.toString())
    EmailServiceImpl.enviarEmailParaSolicitante(sol, StatusSolicitacao.AprovadaDI.toString())
    Redirect(routes.SolicitacoesController.listarSolicitacoes())
  }
  
  def reprovar(id : Long) = authenticatedSecretarioAction {
    val sol = SolicitacaoServiceImpl.getSolicitacao(id)
    SolicitacaoServiceImpl.mudaStatus(sol, StatusSolicitacao.Reprovada.toString())
    EmailServiceImpl.enviarEmailParaSolicitante(sol, StatusSolicitacao.Reprovada.toString())
    Redirect(routes.SolicitacoesController.listarSolicitacoes())
  }
  
  def addSolicitacao() = authenticatedProfessorAction.async { implicit request =>
    SolicitacaoForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => Future.successful(BadRequest(br.ufes.scap.views.html.addSolicitacao(errorForm))),
      solicitacaoForm => {
        val iniAfast = new Timestamp(solicitacaoForm.dataIniAfast.getTime())
        val fimAfast = new Timestamp(solicitacaoForm.dataFimAfast.getTime())
        val iniEvento = new Timestamp(solicitacaoForm.dataIniEvento.getTime())
        val fimEvento = new Timestamp(solicitacaoForm.dataFimEvento.getTime())
        val dataAtual = new Timestamp(Calendar.getInstance().getTime().getTime())
        val newSolicitacao = Solicitacao(0, Global.SESSION_KEY, 0, dataAtual,
            iniAfast, fimAfast, iniEvento, fimEvento, 
            solicitacaoForm.nomeEvento, solicitacaoForm.cidade, solicitacaoForm.onus, solicitacaoForm.tipoAfastamento,
            StatusSolicitacao.Iniciada.toString(), "")
        SolicitacaoServiceImpl.addSolicitacao(newSolicitacao)
        EmailServiceImpl.enviarEmailParaTodos()
        Future.successful(Redirect(routes.SolicitacoesController.listarSolicitacoes()))
      })
  }
  
  def encaminharSolicitacaoForm(idSolicitacao : Long) = authenticatedChefeAction { implicit request =>
    if (Global.isChefe()){
        val users = UsuarioServiceImpl.listAllUsersByTipo(TipoUsuario.Prof.toString())
        val solicitacao = SolicitacaoServiceImpl.getSolicitacao(idSolicitacao)
        Ok(br.ufes.scap.views.html.encaminharSolicitacao(EncaminhamentoForm.form, users, solicitacao))
    }else{
        Ok(br.ufes.scap.views.html.erro())
    }
  }
  
  def encaminharSolicitacao(idSolicitacao : Long) = authenticatedChefeAction.async { implicit request =>
    EncaminhamentoForm.form.bindFromRequest.fold(
        // if any error in submitted data
        errorForm => 
          Future.successful
          (BadRequest
              (br.ufes.scap.views.html.encaminharSolicitacao
                  (errorForm, UsuarioServiceImpl.listAllUsersByTipo(TipoUsuario.Prof.toString()),
SolicitacaoServiceImpl.getSolicitacao(idSolicitacao)
                  )
              )
          ),
        data => {
          val oldSolicitacao = SolicitacaoServiceImpl.getSolicitacao(idSolicitacao)
          val newSolicitacao = SolicitacaoServiceImpl.addRelator(oldSolicitacao, data.idRelator)
          EmailServiceImpl.enviarEmailParaRelator(newSolicitacao, newSolicitacao.relator.get)
          SolicitacaoServiceImpl.mudaStatus(newSolicitacao,StatusSolicitacao.Liberada.toString())
          Future.successful(Ok(br.ufes.scap.views.html.menu(UserForm.form, UsuarioServiceImpl.getUser(Global.SESSION_KEY))))
          
     })
  }
  
}
