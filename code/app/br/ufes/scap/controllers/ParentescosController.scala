package br.ufes.scap.controllers

import br.ufes.scap.models.{Global, Parentesco, ParentescoFull, 
  User, UserForm, ParentescoForm, TipoUsuario}
import play.api.mvc._
import br.ufes.scap.services.{UsuarioServiceImpl, ParentescoServiceImpl}
import scala.concurrent.ExecutionContext.Implicits.global
import javax.inject.Inject
import scala.concurrent.Future
import java.sql.Timestamp
import java.sql.Date
import scala.collection.Seq
import java.util.Calendar
import java.text.SimpleDateFormat

class ParentescosController @Inject() 
(authenticatedUsuarioAction: AuthenticatedUsuarioAction,
    authenticatedSecretarioAction : AuthenticatedSecretarioAction) extends Controller { 
  
  def listarParentescos = authenticatedUsuarioAction { 
        val parentescos = ParentescoServiceImpl.listAllParentescos
        Ok(br.ufes.scap.views.html.listParentescos(parentescos, Global.SESSION_TIPO))
  }
  
  def listarParentescosByProfessor(idProfessor : Long) = authenticatedUsuarioAction { 
        val parentescos = ParentescoServiceImpl.listAllParentescosByProfessor(idProfessor)
        Ok(br.ufes.scap.views.html.listParentescos(parentescos, Global.SESSION_TIPO))
  }

  def deleteParentesco(id: Long) = authenticatedSecretarioAction { implicit request =>
      ParentescoServiceImpl.deleteParentesco(id)
      Redirect(routes.ParentescosController.listarParentescos())
  }
  
  def addParentescoForm() = authenticatedSecretarioAction { implicit request =>
      val users = UsuarioServiceImpl.listAllUsersByTipo(TipoUsuario.Prof.toString())
      Ok(br.ufes.scap.views.html.addParentesco(ParentescoForm.form,users))    
  }
  
  def addParentesco() = authenticatedSecretarioAction.async { implicit request =>
    ParentescoForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => 
        Future.successful
        (BadRequest
          (br.ufes.scap.views.html.addParentesco
             (errorForm, 
                UsuarioServiceImpl.listAllUsersByTipo(TipoUsuario.Prof.toString())
             )
           )
        ),
      data => {
        val newParentesco = Parentesco(0, data.idProfessor1, data.idProfessor2)
        ParentescoServiceImpl.addParentesco(newParentesco)
        Future.successful(Redirect(routes.ParentescosController.listarParentescos()))
      })
  }
 
}
