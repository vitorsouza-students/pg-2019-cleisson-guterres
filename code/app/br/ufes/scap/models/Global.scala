package br.ufes.scap.models

import br.ufes.scap.services.MandatoServiceImpl

@javax.inject.Singleton
object Global {

    var SESSION_KEY : Long = 0
    var SESSION_TIPO : String = ""
    var SESSION_MATRICULA: String = ""
    var SESSION_EMAIL : String = ""
    var SESSION_PASS : String = ""
    var SESSION_CHEFE : Boolean = false
    var CHEFE_ID : Long = 0
    
  
    def isAutor(professor : User) : Boolean ={
       return(professor.id == Global.SESSION_KEY)
    }
    
    def isSecretario() : Boolean ={ 
      return (Global.SESSION_TIPO.equals(TipoUsuario.Sec.toString()))
    }
    
    def isChefe(): Boolean = {
      return Global.SESSION_CHEFE   
    }
    
    def isRelator(relator : Option[User]): Boolean ={
      if (relator == None){
        return false
      }
      return (Global.SESSION_KEY == relator.get.id)
    }
           
    def isProfessor() : Boolean ={ 
      return (Global.SESSION_TIPO.equals(TipoUsuario.Prof.toString()))
    }
    
    def isPresidenteOuVice() = {
      val mandatos = MandatoServiceImpl.getMandatoAtual()
      for (m <- mandatos){
        Global.CHEFE_ID = m.professor.id
        if (Global.SESSION_KEY == m.professor.id){
          Global.SESSION_CHEFE = true
        }
      }
    }
}
