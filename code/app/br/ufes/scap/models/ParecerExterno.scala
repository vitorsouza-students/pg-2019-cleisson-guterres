package br.ufes.scap.models

import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms._
import slick.driver.MySQLDriver.api._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import java.sql.Timestamp
import java.util.Date
import java.time.LocalDate

case class ParecerExterno(id: Long, idSolicitacao : Long, tipo: String, julgamento : String, 
    fileData: Array[Byte], dataParecer : Timestamp)
    
case class ParecerExternoFull(id: Long, solicitacao : SolicitacaoFull, tipo: String, julgamento : String, 
    fileData: Array[Byte], dataParecer : LocalDate)
    
case class ParecerExternoFormData(tipo: String, 
    julgamento : String, filePath: String)
    
object ParecerExternoForm {
  
  val form = Form(
    mapping(
      "tipo" -> nonEmptyText,
      "julgamento" -> nonEmptyText,
      "filePath" -> text
    )(ParecerExternoFormData.apply)(ParecerExternoFormData.unapply)      
  )

}

class ParecerExternoTableDef(tag: Tag) extends Table[ParecerExterno](tag, "parecer_documento") {

  def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
  def idSolicitacao = column[Long]("idSolicitacao")
  def julgamento = column[String]("julgamento")
  def tipo = column[String]("tipo")
  def fileData = column[Array[Byte]]("fileData")
  def dataParecer = column[Timestamp]("dataParecer")

  override def * =
    (id, idSolicitacao,  tipo, julgamento,
       fileData , dataParecer)<>(ParecerExterno.tupled, ParecerExterno.unapply)
}

