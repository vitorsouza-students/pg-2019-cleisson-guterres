\chapter{Fundamentação Teórica e Tecnologias Utilizadas}
\label{capitulo2}

Neste capítulo serão apresentados: a Engenharia Web e suas fases; o método \textit{FrameWeb}, sua arquitetura de software e sua linguagem de modelagem; e os \textit{frameworks} MVC, categoria a qual o  \textit{Play} pertence. Também será feita uma breve introdução a linguagem Scala e ao \textit{framework Play}.

\section{Engenharia Web}
\label{eng-web}

A Internet passou a ser parte integral das nossas vidas: ela está presente nas indústrias, no governo, nos bancos, nas lojas (muitas das vezes, lojas completamente virtuais), entre outros lugares. À medida que as pessoas dependem cada vez mais de sistemas e aplicativos baseados em Web, se torna imprescindível que esses apresentem um padrão alto de qualidade.

A Engenharia Web (\textit{Web Engineering - WebE}) é a Engenharia de Software aplicada ao desenvolvimento Web~\cite{pressmannSoftEng}. Ela estabelece abordagens disciplinadas e sistemáticas, através de um tom científico, técnicas de engenharia e princípios de gestão, para garantir o bom desenvolvimento, implementação e manutenção de sistemas e aplicações baseadas na Web~\cite{murugesan2001web}.

De acordo com \citeonline{olsina2001specifying}, os atributos mais relevantes para a garantia de qualidade nas aplicações Web são:

\begin{itemize}
    \item Usabilidade: refere-se à facilidade de compreensão e uso do site, incluindo usuários com pouco conhecimento técnico e também os que possuem alguma deficiência. Para isso, deve-se empregar recursos de ajuda, \textit{feedback online} e planejamento da interface nos aspectos estéticos e com características especiais voltadas a determinados usuários;

    \item Funcionalidade: refere-se ao funcionamento correto do sistema, isso é, sua capacidade de busca e recuperação de informações, a adaptação aos diferentes \textit{browsers}, e ser capaz de executar as funções relacionadas ao domínio da aplicação do sistema;

    \item Confiabilidade: refere-se à garantia da validação dos dados de entrada dos usuários, recuperação de erros e funcionamento correto dos \textit{links} no site;

    \item Eficiência: refere-se a garantir uma velocidade satisfatória na geração de páginas, fazendo com que o tempo de resposta seja o menor possível;

    \item Manutenibilidade: refere-se à facilidade do sistema em ter seus erros corrigidos, se adaptar a modificações e estender seu uso a novas situações. Esse aspecto é fundamental devido à rápida evolução tecnológica e à necessidade de atualização constante de conteúdo na Web.
\end{itemize}    

A metodologia da Engenharia Web trabalha para que tais atributos sejam plenamente satisfeitos. O modelo proposto pela Engenharia Web é divido em várias fases e segue uma abordagem iterativa. As fases estão descritas a seguir.

\subsection{Análise de Requisitos}

A primeira fase da Engenharia Web é a Análise de Requisitos, etapa na qual tudo o que é considerado necessário para a entrega da aplicação é levantado por meio de consulta aos \textit{stakeholders}, i.e., todas as pessoas interessadas no sistema, incluindo donos e possíveis usuários. São coletadas as informações sobre os requisitos funcionais --- as funções que o sistema deve executar --- e sobre os requisitos não-funcionais --- as restrições sob as quais o sistema deve operar.

Nessa fase, os seguintes questionamentos tem que ser respondidos:

\begin{itemize}
    \item Quais são os objetivos visados pela \textit{WebApp} (aplicação Web)?
    \item Quem serão os usuários da \textit{WebApp}?
    \item Quais necessidades de negócio devem ser atendidas pela \textit{WebApp}?
\end{itemize}

\subsection{Modelo de Análise}

A próxima fase é o Modelo de Análise e foca-se principalmente nas seguintes categorias: \textbf{Conteúdo}, que identifica as classes de conteúdo que devem ser fornecidas para a \textit{WebApp}; \textbf{Interação}, que descreve a forma pelo qual o usuário interage com a \textit{WebApp}; \textbf{Funcional}, que define as operações aplicadas ao conteúdo da Webapp e a sequência de processamentos consequentes; \textbf{Navegação}, que define a estratégia geral de navegação para a \textit{WebApp}; e \textbf{Configuração}, que descreve o ambiente operacional e a infraestrutura na qual a \textit{WebApp} reside.

\subsection{Projeto da \textit{WebApp}}

Com base nos resultados do modelo de análise, a próxima fase da Engenharia Web é o Projeto da \textit{WebApp}, que é focado em seis tópicos fundamentais: \textbf{interface}, \textbf{estética}, \textbf{conteúdo}, \textbf{navegação}, \textbf{arquitetura} e {\textbf{componentes}.

\subsection{Implementação}

Na fase de implementação, o projeto é propriamente desenvolvido utilizando uma linguagem de programação adequada às suas características e requisitos. 

\subsection{Testes}

Por fim, a fase de testes visa garantir que a \textit{WebApp}  esteja correta, principalmente em termos de conteúdo (isto é, atende aos requisitos levantados), navegabilidade, segurança, carga, eficiência e interoperabilidade entre diferentes navegadores Web.

\section{FrameWeb}

O FrameWeb~\cite{Souza2007} (\textit{Framework-based Design Method for Web Engineering}) é um método baseado em \textit{frameworks} para o desenvolvimento de sistemas de informação Web (\textit {Web Information System} – WIS). Dentre as diversas propostas para Engenharia Web, até então não havia nenhuma que considerasse diretamente os aspectos característicos dos \textit{frameworks} na construção de WISs, porém o FrameWeb apresenta essa abordagem. 

O método não prescreve nenhum processo de desenvolvimento de software específico. A fase de projeto arquitetural concentra as propostas principais do método, a saber: 

\begin{itemize}
    \item{definição de uma arquitetura padrão que divide o sistema em camadas, para promover sua melhor integração com os \textit{frameworks} utilizados;}
    \item{proposição de um conjunto de modelos de projeto que empregam conceitos utilizados pelos \textit{frameworks} por meio de um perfil UML que aproxima os diagramas da implementação}
\end{itemize}

Na fase de implementação a produção do código é agilizada pela utilização dos \textit{frameworks} e pela fidelidade que existe entre os modelos da fase de projeto e sua implementação.

\subsection{Arquitetura de software do FrameWeb}

A arquitetura lógica padrão para WISs utilizada no FrameWeb é apresentada na Figura~\ref{arquitetura-software}. O sistema é dividido em três camadas, que apresentam subdivisões internas em pacotes:

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{figuras/fig-arquitetura-software.jpg}
	\caption{Arquitetura padrão para WIS baseada no padrão arquitetônico ServiceLayer~\cite{fowler2002patterns}.}
	\label{arquitetura-software}
\end{figure}

\begin{itemize}
    \item Camada de \textbf{Lógica de Apresentação}: %provê as interfaces gráficas para a interação do usuário com o sistema. Nela são apresentadas as informações das classes de domínio e o usuário faz suas requisições.
        \begin{itemize}
            \item Pacote de \textbf{Visão}: é onde estão páginas Web, imagens e scripts que são executados do lado do cliente, e também os arquivos exclusivamente relacionados com apresentação de informações aos usuários.
            \item Pacote de \textbf{Controle}: é responsável pelo monitoramento dos estímulos enviados pelo cliente através dos elementos da Visão, e também abrange as classes de controle e outros arquivos relacionados ao \textit{framework} Controlador Frontal.
        \end{itemize}
    \item Camada de \textbf{Lógica de Negócio}:
        \begin{itemize}
            \item Pacote de \textbf{Domínio}: nele encontram-se as classes que representam o domínio do problema, já modeladas nos diagramas de classe na fase de análise.
            \item Pacote de \textbf{Aplicação}: nele estão implementados os casos de uso, provendo uma camada de serviços independente da interface com o usuário.
        \end{itemize}
    \item Camada de \textbf{Lógica de Acesso a Dados}:
        \begin{itemize}
            \item Pacote de \textbf{Persistência}: contém as classes responsáveis por armazenar os objetos persistentes em um banco de dados. Tais classes seguem o padrão de projeto \textit{Data Access Object}, ou DAO.
        \end{itemize}

    \end{itemize}

Acerca das relações de dependência entre pacotes, tem-se que:

\begin{itemize}
    \item O pacote de \textbf{Aplicação} manipula objetos do pacote de \textbf{Domínio};
    \item O pacote de \textbf{Aplicação}, por meio do pacote \textbf{Persistência}, recupera, grava, altera e exclui objetos de domínio de acordo com a execução dos casos de uso;
    \item Elementos do pacote de \textbf{Visão} enviam os estímulos do usuário (clique de um botão, preenchimento de um campo de texto, acionamento de uma caixa de seleção, etc) para o pacote de \textbf{Controle}, que por sua vez responde aos estímulos;
    \item O pacote de \textbf{Controle}, que provê ao usuário acesso às principais funcionalidades, possui dependência com o pacote de \textbf{Aplicação};
    \item Os pacotes de \textbf{Visão, Controle e Persistência} possuem uma dependência fraca com o pacote de \textbf{Domínio}: não fazem alterações nos objetos de domínio, apenas os exibem ou os usam para enviar informações nas chamadas de métodos.
\end{itemize}

\subsection{Linguagem de modelagem de FrameWeb}

O método FrameWeb apresenta uma linguagem de modelagem baseada em UML cujo propósito é guiar a implementação das camadas explicadas na seção anterior, a fim de representar componentes utilizados no desenvolvimento Web e componentes relacionados com o uso de \textit{frameworks}. São propostos quatro tipos de diagrama:

\begin{itemize}
    \item O \textbf{Modelo de Entidades} representa os objetos que fazem parte do domínio do problema e seu mapeamento para a persistência. Ele é desenvolvido a partir do diagrama de classes criado na fase de análise.
    \item O \textbf{Modelo de Persistência} representa os objetos que são responsáveis pela persistência dos dados gerados pelo sistema; isto é, guia a implementação das classes DAO do sistema, as interfaces e suas implementações, juntamente com os métodos específicos de cada uma.
    \item O \textbf{Modelo de Aplicação} representa as classes de serviço, que são responsáveis por implementar a lógica de negócio do sistema, e suas dependências.
    \item O \textbf{Modelo de Navegação} representa as páginas Web do sistema e seus atributos, que interagem com a camada de controle do sistema para  administrar os estímulos mandados e recebidos do usuário.
    \label{modelos-basicos}
\end{itemize}

\subsection{Avanços no FrameWeb: Metamodelos, Editor e Gerador de Código}

Foram propostos metamodelos para definir formalmente a linguagem de modelagem do FrameWeb~\cite{Martins2016}, o que permite validar os modelos e construir outras ferramentas baseadas nesse meta-modelo, além de permitir que o método seja estendido para outros \textit{frameworks} além dos originalmente propostos originalmente por \citeonline{Souza2007}.

Baseado nesses metamodelos, foi desenvolvida uma ferramenta CASE chamada Editor FrameWeb, que provê um ambiente gráfico para a criação de modelos FrameWeb~\cite{campos2017}. O que o diferencia de editores UML de propósito geral é que essa ferramenta é totalmente voltada para as características e propriedades do FrameWeb. O editor faz verificações no modelo e impede ações inválidas do ponto de vista do método FrameWeb como, por exemplo, criação de classes fora de pacotes, ou associações indevidas entre componentes, além de não serem disponibilizandos conceitos da UML que não fazem parte do método FrameWeb.

Um passo além foi dado com o Gerador de Código FrameWeb, criado para executar a geração automática de arquivos de código para um WIS modelado no Editor FrameWeb~\cite{almeida2017}. A entrada do Gerador de Código é um arquivo \textit{.frameweb} produzido no Editor FrameWeb e a saída são os arquivos fonte de código para o sistema. Visou-se criar uma ferramenta flexível, capaz de gerar código para diversas linguagens e \textit{frameworks}. O objetivo principal é reduzir o trabalho manual do programador com boa parte do código.

\section{Frameworks MVC}

Para garantir a manutenibilidade e permitir maior escalabilidade, é fundamental que \textit{WebApps} complexas façam uma separação entre os dados (\textit{Model}) e o layout (\textit{View}). Desse modo, mudanças realizadas no layout das páginas não afetam os dados, e mudanças nos dados não afetam o layout.

O MVC (\textit{Model-View-Controller}) resolve este problema através da separação das tarefas de acesso aos dados e lógica de negócio, lógica de apresentação e de interação com o utilizador, introduzindo um componente entre os dois: o controlador. A Figura~\ref{mvc1} ilustra a arquitetura MVC.

\begin{figure}
	\centering
	\includegraphics[width=.7\textwidth]{figuras/fig-padrao-mvc.jpg}
	\caption{Diagrama representando a arquitetura MVC.}
	\label{mvc1}
\end{figure}

Os elementos da \textit{View} (Visão) representam as informações do \textit{Model} (Modelo) para os usuários que, a partir disso, interagem com a aplicação. Essa interação é tratada pelo \textit{Controller} (Controlador), capaz de modificar elementos do Modelo, e também notifica a Visão dessas alterações, que então atualiza a informação apresentada ao usuário.

Porém, na plataforma Web a arquitetura MVC precisa ser levemente adaptada, a esse padrão aplicado a Web dá-se o nome de \textit{Front Controller} (Controlador Frontal), e sua arquitetura é ilustrada na Figura~\ref{mvc2}

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{figuras/fig-controlador-frontal-web.jpg}
	\caption{Representação de um Controlador Frontal na Web~\cite{Souza2007}.}
	\label{mvc2}
\end{figure}

O navegador apresenta as páginas para o cliente, que faz uma requisição HTTP --- um pedido de leitura de uma página ou envio de dados para processamento --- ao servidor. O servidor Web delega a requisição ao controlador frontal de tratar a requisição e este passa a gerenciar todo o processo (lê a configuração, instancia e executa uma ação). Então é emitida uma resposta apropriada à requisição. O resultado é delegado a uma tecnologia de visão para ser exibido para o cliente pelo navegador. Os \textit{frameworks} MVC fornecem um controlador frontal a ser configurado pelo desenvolvedor para melhor adaptação ao seu projeto.

\subsection{Framework Play}

Play é um \textit{framework} MVC \textit{open-source} para aplicações Web, escrito em Scala mas também usável em Java, de alta produtividade, que integra componentes e APIs para o desenvolvimento moderno de aplicativos da Web~\cite{Playdocumentation}. 

Entre suas características, podemos citar que o \textit{Play} é totalmente RESTful; isto é, ele segue os princípios da Transferência de Estado Representacional, usando o protocolo HTTP de forma explícita e representativa para se comunicar em todas as requisições. Além disso, não possui estado entre comunicações, ou seja, cada comunicação é independente e uniforme (padronizada) e passa toda a informação necessária; isto torna o \textit{Play} mais escalável que outros \textit{frameworks}. Também apresenta I/O assíncrono, conseguindo disponibilizar e tratar uma enorme quantidade de requisições assíncronas.

Outros detalhes mais particulares do \textit{framework Play} serão discutidos no Capítulo~\ref{sec-projeto} dentro do contexto da implementação do projeto.

\section{Linguagem Scala}

Scala é uma linguagem que combina orientação a objetos e programação funcional em uma linguagem concisa de alto nível. Sua tipagem estática ajuda a evitar bugs em aplicações complexas. Sua interoperabilidade com Java também permite ao usuário construir sistemas de alta performance com fácil acesso a imensos ecossistemas de bibliotecas~\cite{Scaladocumentation}.  

Detalhes mais particulares da linguagem Scala serão discutidos no Capítulo~\ref{sec-projeto} dentro do contexto da implementação do projeto.

