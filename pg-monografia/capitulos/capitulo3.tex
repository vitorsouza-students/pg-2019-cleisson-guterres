\chapter{O Sistema SCAP}
\label{sec-requisitos}

O  SCAP é um aplicativo Web cujo objetivo é apoiar o Departamento de Informática (DI) da UFES no gerenciamento e registro de solicitações de afastamento de seus professores para eventos no Brasil ou no exterior.

Para eventos realizados no Brasil, o processo corre internamente no DI. Os funcionários do departamento são informados por e-mail, e caso ninguém apresente um parecer contrário num prazo de dez dias, o pedido é aprovado. 

No caso de uma solicitação de afastamento para um evento no exterior, é escolhido um professor que não possua relação de parentesco com o solicitante para ser o relator do pedido. Após a emissão do parecer do relator, o processo é avaliado pelo DI no mesmo molde do caso acima. Caso o pedido seja aprovado no DI, ele é então encaminhado para o Centro Tecnológico (CT) e para a Pró-reitoria de Pesquisa e Pós-Graduação (PRPPG), e, caso aprovado, o afastamento é publicado no Diário Oficial da União. O SCAP é uma ferramenta a ser utilizada dentro do DI, portanto, decisões competentes a outras instâncias seriam apenas transmitidas no sistema por um secretário do DI.

Baseado nos requisitos já levantados por \citeonline{duarte-2014} e \citeonline{prado-2015}, este capítulo apresenta os modelos de casos de uso e diagramas de classe referentes ao sistema SCAP.

\section{Modelos de Casos de Uso}
\label{sec-requisitos-uc}

A Tabela~\ref{tbl-atores-SCAP} lista e descreve os atores identificados.

\begin{table}[h!]
	\caption{Atores do SCAP.}
	\label{tbl-atores-SCAP}
	\centering
	\begin{tabular}{ | p{40mm} | p{100mm} | }
		\hline
		\textbf{Ator} & \textbf{Descrição}  
		\\\hline
		Usuário & Qualquer usuário do sistema
		\\\hline
		Professor & Professores efetivos do DI/UFES. \\
		\hline
		Chefe do Departamento & Professores do DI/UFES que estão realizando a função
		administrativa de chefe e sub-chefe do departamento. \\
		\hline
			Secretário & Secretário do DI/UFES. \\
		\hline
	\end{tabular}
\end{table}


Os \textbf{secretários} são responsáveis pela parte administrativa do sistema. Fazem o cadastro de professores, registram as relações de parentesco ente eles e fazem o cadastro dos mandato de chefe do departamento, podendo excluir relações de parentescos ou mandatos cadastrados erroneamente. O secretário também é responsável por integrar o sistema com setores externos ao DI, cadastrando pareceres provenientes do CT e da PRPPG. Além disso, eles arquivam solicitações de afastamento já finalizadas.

Os \textbf{professores} cadastram suas próprias solicitações de afastamento no sistema, e podem se manifestar contrariamente a solicitações de afastamento de outros professores dentro do prazo. Se um professor é designado para ser relator de uma solicitação de afastamento internacional, ele deve emitir um parecer favorável ou contrário à tal solicitação. 

\textbf{Chefe de departamento} é uma função exercida por um professor durante um mandato temporário. É incumbência do chefe de departamento designar relatores para solicitações de afastamento internacional.

\textbf{Usuário} é qualquer usuário do sistema, seja ele professor --- chefe de departamento ou não --- ou secretário.

Na Figura~\ref{diagrama-scap} é apresentado o diagrama de casos de uso do SCAP.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.95\textwidth]{figuras/diagrama-scap} 
	\caption{Diagrama de Casos de Uso do SCAP.}
	\label{diagrama-scap}
\end{figure}

Qualquer \textbf{usuário} do sistema pode \textbf{Visualizar Afastamentos}, \textbf{Visualizar Pareceres}, \textbf{Visualizar Parentescos}, \textbf{Visualizar Professores}, \textbf{Buscar Afastamentos} por professor, status ou relator, e \textbf{Editar seu Perfil}.

No caso de uso \textbf{Solicitar Afastamento}, o professor (autor do pedido) deve preencher um formulário, informando o nome do evento, o período de afastamento, o período do evento, o motivo do afastamento, o ônus para a instituição, informar se o evento é no Brasil ou no exterior e a cidade onde será realizado. Também pode fazer o \textit{upload} de documentos pertinentes. A solicitação é cadastrada no sistema e os professores do DI são notificados a respeito dela pelo recebimento de um e-mail automático.

Nesse ponto, outro professor pode \textbf{Manifestar-se Contra Afastamento} e, nesse caso, após uma reunião do DI, o Secretário \textbf{Registra Aprovação do Afastamento} ou \textbf{Registra Reprovação do Afastamento}. Caso nenhum professor se manifeste durante o prazo de dez dias, a solicitação é automanticamente aprovada pelo DI.

No caso de solicitações para eventos no Brasil, o processo segue como descrito acima. Porém, caso a solicitação seja para um evento fora do Brasil, existem outras etapas adicionais além dessas. Assim que a solicitação é cadastrada no sistema, um e-mail automaticamente é enviado ao chefe do departamento, que deve \textbf{Encaminhar o Afastamento} para outro professor do DI de sua escolha que atuará como relator (também notificado automaticamente por e-mail).

O relator vai preencher um formulário para \textbf{Deferir Parecer}. O parecer é cadastrado, o sistema gera um documento contendo esse parecer e o professor que fez a solicitação é notificado por e-mail. Caso o parecer seja negativo, uma reunião do DI é marcada para decidir se a solicitação será realmente reprovada.

A qualquer momento, o professor que solicitou o afastamento pode \textbf{Cancelar Afastamento}. Um e-mail é enviado notificando o(a) chefe do departamento sobre o cancelamento.

Um secretário pode \textbf{Registrar Usuários}, informando nome, matrícula, e-mail, senha e tipo de usuário (professor ou secretário). Pode também \textbf{Registrar Mandatos}, registrando um professor já previamente cadastrado como chefe ou subchefe de departamento, e informando a data de início e fim previsto do seu mandato.

Para casos de solicitações de afastamento relativas a eventos no exterior, fica a cargo dos secretários \textbf{Registrar Pareceres Externos}, do CT e da PRPPG. Em ambos os casos, o secretário cadastra o parecer e faz \textit{upload} dos documentos enviados contendo o parecer. Baseado no resultado do parecer, o status do afastamento é alterado para reprovado ou aprovado.

Quando o afastamento termina e o professor retorna, o  Secretário tem a incumbência de \textbf{Arquivar Afastamento}, alterando o status da solicitação.

\section{Modelo de Classes}
\label{sec-requisitos-analise}

A primeiro etapa na análise de requisitos é modelar as entidades do mundo real dentro do paradigma de Orientação a Objetos. As entidades são descritas por classes, suas características são manifestas nos atributos das classes, e as interações entre elas são representadas pelas relações entre as classes. É fundamental destacar a multiplicidade de relações entre as classes, o que deve ser retratado na persistência de dados.

O Modelo de Classes do SCAP é apresentado na Figura~\ref{fig-diagrama-de-classes}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.9\textwidth]{figuras/fig-diagrama-de-classes}
	\caption{Diagrama de Classes do SCAP.}
	\label{fig-diagrama-de-classes}
\end{figure}

As classes \textbf{Professor} e \textbf{Secretário} são subclasses de \textbf{Usuario} e representam os professores e secretários do DI. 

A classe \textbf{Parentesco} é usada para representar relação de parentesco --- sanguínea ou matrimonial --- entre professores. Essa classe não possui atributos pois, como o foco do trabalho são os afastamentos e não os professores, não foi considerado importante guardar a informação de qual exatamente é o parentesco entre dois professores. Basta saber que, havendo um parentesco de qualquer tipo, um professor fica inelegível para ser relator da solicitação de afastamento de outro professor.

A classe \textbf{Mandato} representa o mandato de um professor designado para o cargo de chefe ou subchefe de departamento.

A classe \textbf{Afastamento} representa um pedido de afastamento solicitado por um professor. Se a solicitação for para um evento no exterior,  é necessário que outro professor seja designado \textbf{Relator} e seu parecer é descrito pela classe \textbf{Parecer}. Cada parecer é emitido por um único professor em relação a um único afastamento. Embora um professor possa emitir vários pareceres para vários afastamentos diferentes. Uma manifestação contrária a um evento nacional é representada também por um parecer.

Os pareceres advindos do CT e da PRPPG são representados pela classe \textbf{Parecer Externo}. Esses pareceres possuem um documento associado (representado por um atributo da classe).

Feito o diagrama de classes, a próxima etapa é listar as restrições de integridade, que consistem em relacionamentos entre as entidades que não podem ser representados no diagrama de classes, mas são fundamentais para a compreensão e implementação do sistema.  As restrições de integridade pertencentes ao sistema SCAP são listadas a seguir:

\begin{itemize}
	\item Um professor não pode ser relator de um afastamento solicitado por um parente ou por ele mesmo;
	\item Não pode haver mais de dois professores (chefe e subchefe de departamento) exercendo um mandato simultaneamente;
	\item A data de início de um afastamento não pode ser posterior à data de fim do mesmo afastamento;
	\item A data de início de um mandato de professor não pode ser posterior à data de fim do mesmo mandato;
	\item Secretários do departamento não podem criar solicitação de afastamento;
	\item Um mesmo usuário do SCAP não pode ser professor e secretário.
	
\end{itemize}

\label{capitulo-scap}
